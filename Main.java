import java.util.Scanner;

public class Main {
  public static void main(String []args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter name of the Acid: ");
    String name = sc.nextLine();
    System.out.println("Enter density of the Acid: ");
    float density = sc.nextFloat();
    System.out.println("Enter concentration of the Acid: ");
    float concentration = sc.nextFloat();
    var acid = new Acid(name, density, concentration);
    acid.showFields();

    sc.nextLine();
    System.out.println("Change name of the Acid: ");
    acid.setName(sc.nextLine());
    System.out.println("Change density of the Acid: ");
    acid.setDensity(sc.nextFloat());
    System.out.println("Change concentration of the Acid: ");
    acid.setConcentration(sc.nextFloat());
    acid.showFields();
    sc.close();
  }
}
