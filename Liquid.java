class Liquid {
  String name;
  float density;

  Liquid() {
    this.name = "aqua";
    this.density = 997;
  }

  /**
   * Конструктор для объекта класса "Жидкость".
   * @param name Название жидкости.
   * @param density Плотность жидкости в г/см^3.
   */
  Liquid(String name, float density) {
    this.name = name;
    this.density = density;
  }

  void setName(String name) {
    this.name = name;
  }

  void setDensity(float density) {
    this.density = density;
  }

  String getName() {
    return name;
  }

  float getDensity() {
    return density;
  }

  void showFields() {
    System.out.println("\nName: " + name);
    System.out.println("Density: " + density + " g/cm^3");
  }
}
