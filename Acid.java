class Acid extends Liquid{
  float concentration;

  Acid() {
    super();
    this.concentration = 0;
  }

  /**
   * Конструктор для объекта класса "Кислота."
   * @param name Название кислоты.
   * @param density Плотность кислоты в г/см^3.
   * @param concentration Концентрация кислоты в %.
   */
  Acid(String name, float density, float concentration) {
    super(name, density);
    this.concentration = concentration;
  }

  void setConcentration(float concentration) {
    this.concentration = concentration;
  }

  float getConcentration() {
    return concentration;
  }

  void showFields() {
    super.showFields();
    System.out.println("Concentration: " + concentration + "%");
  }
}
